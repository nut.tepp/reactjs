import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";
import Time from "./Time"


class App extends Component {
  render() {
    return (
      <div className="App">
        <div class="container-fluid">
          <div class="row justify-content-md-center">
            <div class="col-lg-6 col-md-12 col-sm-12 h03">
              <div class="row">
                <div id="header" class="col-12 mt-3">
                  <div class="row align-items-center">
                    <div class="col">
                      <h2>TODO Lists</h2>
                    </div>
                    <div class="col text-right">
                      <h3>30 Feb 2019</h3>
                      <h5 class="mt-0"><Time></Time></h5>
                    </div>
                    <div id="completeSection" class="col-12" />
                    <div id="id" class="col-12" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
